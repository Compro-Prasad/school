from django.core.exceptions import ValidationError
from django.test import TestCase
from student.models import StudentProfile


class TestStudentProfile(TestCase):
    """Test StudentProfile model."""
    def test_aadhaar(self):
        profile = StudentProfile(aadhaar="")
        import pdb; pdb.set_trace()
        self.assertRaises(ValidationError, profile.clean_aadhaar)
