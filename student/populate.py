from datetime import date
from random import sample, randint, choice

import stdnum
from stdnum.in_ import aadhaar

from .models import Student, StudentProfile, FeeStructure, Session
from .models import StudentTransaction, ParentProfile, Discount
from .models import ParentTransaction


class Populate:
    FIRST_NAMES = {
        "F": [
            "Sheetal", "Naina", "Kajal", "Kajol", "Alisha", "Aisha",
            "Aindrilla", "Aindrila", "Oindrila", "Oindrilla", "Ananya", "Avni",
            "Olivia", "Alivia", "Drishti", "Ishita", "Isheeta", "Navya",
            "Niharika", "Sneha", "Neha", "Arohi", "Aditi", "Aisha", "Binita",
            "Bodhi", "Chaitali", "Chetana", "Chetna", "Shreshtha", "Damini",
            "Debanjali", "Deepali", "Deeksha", "Diya", "Eesha", "Idika",
            "Juhi", "Indu", "Khushi", "Lata", "Lalita", "Noor", "Oorja",
            "Pari", "Puja", "Pooja", "Chetali", "Indu", "Indumati",
            "Indulekha", "Usha", "Prerna", "Parna", "Lakhi", "Basmati",
            "Lakshmi", "Parvati", "Sheela", "Shanti", "Shridevi", "Mary",
            "Archana", "Aradhna", "Abhipsha", "Sumouli", "Soumili", "Deepika",
            "Katrina", "Pragya", "Sabita", "Savita", "Shraddha", "Nargis",
            "Katlina", "Shushmita", "Samaya", "Ruchita", "Dipika", "Tanya",
            "Garima", "Nidhi", "Riya", "Muskaan", "Muskan", "Manorama",
            "Senorita", "Shyamrathi", "Rambasi", "Dhanesari", "Kusum"
        ],
        "M": [
            "Soham", "Rahul", "Biswajeet", "Viswajeet", "Akash", "Aakash",
            "Oishik", "Aishik", "Aaishik", "Druv", "Alex", "Raj", "Raja",
            "Raunak", "Rajesh", "Shankar", "Shiv", "Shiva", "Shiw", "Ritwik",
            "Rittik", "Ritvik", "Tushar", "Tushaar", "Vaibhav", "Nilendra",
            "Suraj", "Sooraj", "Gourav", "Gaurav", "Gourab", "Gaurab",
            "Awadhesh", "Ram", "Rahim", "Abhishek", "Abhinaw", "Abhinav",
            "Abhisek", "Avisek", "Avishek", "Rajan", "Deepansh", "Parmindar",
            "Indrajit", "Indrajeet", "Rambadan", "Ronit", "Deepak", "Ranjit",
            "Manish", "Rohit", "Braj", "Brajraj", "Kailash", "Brahma",
            "Vishnu", "Nihal", "Mrudul", "Shambhu", "Lalu", "Haquib", "Amir",
            "Amer", "Roshan", "Sanjay", "Balchand", "Tanmay", "Varun",
            "Ashish", "Aqueeb", "Jeet", "Dhruv", "Bator", "Sudershan",
            "Vidyut", "Vedant", "Abhay", "Aditya", "Atul", "Ajit", "Ashutosh",
            "Akhilesh", "Angad", "Brijendra", "Dinesh", "Gajraj", "Harsh",
            "Kabir", "Kunal", "Kartick", "Kartik", "Karthick", "Karthik",
            "Kishore", "Mukesh", "Rajat", "Ranveer", "Ranbir", "Sohail",
            "Siddhart", "Sidhart", "Siddharth", "Sudhir", "Siddhant", "Sharad",
            "Sumit", "Sumeet"
        ]
    }
    LAST_NAMES = ["Nath", "Rai", "Roy", "Ray", "Prasad", "Prajapati",
                  "Aggarwal", "Aggarwaal", "Kapur", "Kapoor", "Goel", "Madan",
                  "Malik", "Sen", "Sain", "Khan", "Arab", "Thakur", "Kohli",
                  "Biswas", "Chaterjee", "Chakraborty", "Banerjee", "Bhat",
                  "Bhatt", "Bali", "Basu", "Choudhary", "Choudhry",
                  "Choudhury", "Chowdhury", "Dalal", "Dixit", "Mishra",
                  "Misra", "Chawla", "Singh", "Das", "Ulla", "Dutt", "Yadav",
                  "Rab", "Bhagwan", "Roshan", "Jain", "Dravid", "Ganguly",
                  "Tendulkar", "Akhtar", "Sharma", "Bachchan", "Gill",
                  "Shetty", "Kher", "Desai", "Nath", "Palekar", "Bedi",
                  "Tiwari", "Srivastav", "Shrivastav", "Hussain", "Pratap",
                  "Mahajan", "Deol", "Pancholi", "Devgan", "Joshi", "Pandey"]

    def get_dob(session, std):
        year = session.start.year
        if std == "LKG":
            year -= 4
        elif std == "UKG":
            year -= 5
        else:
            year -= 4 + Student.ROMAN2INT[std]
        return date(day=randint(1, 28), month=randint(1, 12), year=year)

    def get_adhaar_numbers(n, old_set=set()):
        adhaars = set(old_set)
        new_adhaars = set()
        while len(new_adhaars) < n:
            random_adhaar = "".join(sample("123456789", k=9) +
                                    sample("123456789", k=3))
            try:
                random_adhaar = aadhaar.validate(random_adhaar)
            except (stdnum.exceptions.InvalidFormat,
                    stdnum.exceptions.InvalidLength,
                    stdnum.exceptions.InvalidChecksum,
                    TypeError):
                continue
            if random_adhaar not in adhaars:
                adhaars.add(random_adhaar)
                new_adhaars.add(random_adhaar)
        return new_adhaars

    def delete(self):
        ParentTransaction.objects.all().delete()
        StudentTransaction.objects.all().delete()
        Discount.objects.all().delete()
        Student.objects.all().delete()
        FeeStructure.objects.all().delete()
        StudentProfile.objects.all().delete()
        ParentProfile.objects.all().delete()
        Session.objects.all().delete()

    def __init__(self):
        print("Started population")
        self.delete()
        print("Deleted all")
        parent_count = randint(5, 10) * 7
        print(f"parent_count = {parent_count}")
        aadhaar_nos = Populate.get_adhaar_numbers(1000)
        father_first = Populate.get_first_names("M", parent_count)
        mother_first = Populate.get_first_names("F", parent_count)
        last_names = set(Populate.LAST_NAMES)
        surnames = []
        while len(surnames) < parent_count:
            surnames += sample(Populate.LAST_NAMES, k=7)
            last_names -= set(sample(surnames, k=3))

        parents = [
            ParentProfile(
                father_name=father_first[i] + " " + surname,
                father_aadhaar=aadhaar_nos.pop(),
                mother_name=mother_first[i] + " " + surname,
                mother_aadhaar=aadhaar_nos.pop()
            )
            for i, surname in enumerate(surnames)
        ]
        ParentProfile.objects.bulk_create(parents)
        parents = ParentProfile.objects.all()

        print("Created parent profiles")

        sessions = [
            Session(
                start=date(2010 + i, 4, 1),
                end=date(2010 + i + 1, 3, 31),
            )
            for i in range(10)
        ]
        Session.objects.bulk_create(sessions)
        sessions = Session.objects.all()

        print("Created sessions")

        self.make_fee_structs(sessions)
        self.make_discounts(sessions)

        student_profiles = []
        students = []
        scholar_no = 100
        students_count = {}
        students_count["F"] = randint(20, 40)
        students_count["M"] = 80 - students_count["F"]
        for parent in parents:
            children_count = randint(1, 4)
            while children_count != 0:
                gender = Populate.get_gender()
                category = Populate.get_category()
                first_name = Populate.get_first_name(gender[0])
                surname = parent.father_name.split()[1]
                std = choice(Student.CLASSES)
                session = sessions[0]
                dob = Populate.get_dob(session=session, std=std)
                student_profiles.append(
                    StudentProfile(
                        aadhaar=aadhaar_nos.pop(),
                        name=first_name + " " + surname,
                        parents=parent,
                        gender=gender,
                        date_of_birth=dob,
                        category=category,
                        phone="9713898127"
                    )
                )
                students.append(
                    Student(
                        scholar_number=scholar_no,
                        std=std,
                        session=session,
                    )
                )
                scholar_no += 1
                children_count -= 1
        StudentProfile.objects.bulk_create(student_profiles)
        print("Created student profiles")
        for i, student in enumerate(StudentProfile.objects.all()):
            students[i].profile = student
        Student.objects.bulk_create(students)
        print("Created students")

    def make_discounts(self, sessions):
        check = "student.get_students('parent_children').count() >= 3"
        Discount.objects.bulk_create([
            Discount(
                short_code="3CHILD",
                eligibility_check=check,
                auto_apply_check="False",
                discount=50,
                session=session
            ) for session in sessions
        ])
        print("Created discounts")

    def make_fee_structs(self, sessions):
        fee_structs = []
        increase_by = [0, 10, 0, 10, 0, 20, 0, 30, 10, 0,
                       0, 20, 40, 0, 0, 30, 10, 50, 0, 0]
        mth_start = 40
        adm_start = 50
        exm_start = 30
        prl_start = 100
        for session in sessions:
            mth_tmp = mth_start
            adm_tmp = adm_start
            exm_tmp = exm_start
            for std in Student.CLASSES:
                fee_structs.append(FeeStructure(
                    session=session,
                    std=std,
                    admission_fees=adm_tmp,
                    monthly_fees=mth_tmp,
                    exam_fees=exm_tmp
                ))
                adm_tmp += choice([x for x in increase_by if x < 30])
                mth_tmp += choice([x for x in increase_by if x < 30])
                exm_tmp += choice([x for x in increase_by if x < 30])
                if std in ('X', 'XII'):
                    fee_structs[-1].practical_fees = prl_start
                    prl_start += choice(increase_by)
            mth_start += choice(increase_by)
            adm_start += choice(increase_by)
            exm_start += choice(increase_by)
        FeeStructure.objects.bulk_create(fee_structs)
        print("Created fee structures")

    def get_gender():
        return choice(["Male", "Female"])

    def get_category():
        return choice([x for x, _ in StudentProfile.CATEGORY_CHOICE])

    def get_first_names(gender, count):
        return sample(Populate.FIRST_NAMES[gender], k=count)

    def get_first_name(gender):
        return choice(Populate.FIRST_NAMES[gender])
