from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import SimpleListFilter
from .models import (
    StudentProfile, Session, Student, FeeStructure, StudentTransaction,
    ParentProfile, Discount, ParentTransaction, Remarks
)
from .forms import SessionForm, FeeStructureForm, StudentForm


class StudentProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('img',)
    search_fields = ('aadhaar', 'phone', 'name')


class FeeStructureAdmin(admin.ModelAdmin):
    form = FeeStructureForm
    list_filter = ['session', 'std', 'admission_fees',
                   'monthly_fees', 'exam_fees']
    list_display = ['__str__', 'admission_fees', 'monthly_fees', 'exam_fees']

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if not obj:
            return False
        return not StudentTransaction.objects.filter(
            student__std=obj.std, student__session=obj.session)


class SchoolFilter(SimpleListFilter):
    title = _('School')

    parameter_name = 'school'

    def lookups(self, request, model_admin):
        return [(None, _('All'))] + [
            (school, _(school)) for school in Student.SCHOOLS
        ]

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() is None:
            return queryset
        if self.value() in Student.SCHOOLS:
            return queryset.filter(std__in=Student.SCHOOL2CLASS[self.value()])


def admin_method_attributes(**outer_kwargs):
    """Wrap an admin method with passed arguments as attributes and values. DRY way
    of extremely common admin manipulation such as setting short_description,
    allow_tags, etc.
    """
    def method_decorator(func):
        for kw, arg in outer_kwargs.items():
            setattr(func, kw, arg)
        return func
    return method_decorator


class StudentAdmin(admin.ModelAdmin):
    form = StudentForm
    raw_id_fields = ('profile',)
    search_fields = (
        'profile__aadhaar', 'profile__phone', 'profile__name'
    )
    list_filter = [SchoolFilter, 'status', 'std', 'session']
    list_display = ['__str__']


class SessionAdmin(admin.ModelAdmin):
    form = SessionForm

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if not obj:
            return False
        return not StudentTransaction.objects.filter(student__session=obj)


class StudentTransactionAdmin(admin.ModelAdmin):
    readonly_fields = ['student', 'received', 'returned']
    list_display = ('__str__', 'received', 'returned', 'giver_gave',
                    'created_at', 'updated_at')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class ParentTransactionAdmin(admin.ModelAdmin):
    readonly_fields = ['parents', 'received', 'returned']
    list_display = ('parents', 'received', 'returned', 'giver_gave',
                    'created_at', 'updated_at')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class RemarksAdmin(admin.ModelAdmin):
    list_display = ('student_prev', 'student_next', 'summary')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class DiscountModelAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'session', 'max_discount_amount',
                    'min_discount_amount')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(StudentProfile, StudentProfileAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(ParentProfile)
admin.site.register(Remarks, RemarksAdmin)
admin.site.register(Discount, DiscountModelAdmin)
admin.site.register(StudentTransaction, StudentTransactionAdmin)
admin.site.register(ParentTransaction, ParentTransactionAdmin)
admin.site.register(FeeStructure, FeeStructureAdmin)
