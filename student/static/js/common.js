function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    var data = dataURI.split(',');
    var byteString = (data[0].indexOf('base64') >= 0 ? atob : unescape)(data[1]);

    // separate out the mime component
    var mimeString = data[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function humanReadableSize(bytes) {
    var blob_unit = "bytes";
    if (bytes < 1024);  // Do nothing
    else if (bytes < 1024 * 1024) {
        bytes /= 1024;
        blob_unit = "KB";
    } else if (bytes < 1024 * 1024 * 1024) {
        bytes /= 1024 * 1024;
        blob_unit = "KB";
    } else if (bytes < 1024 * 1024 * 1024 * 1024) {
        bytes /= 1024 * 1024 * 1024;
        blob_unit = "GB";
    } else {
        bytes /= 1024 * 1024 * 1024 * 1024;
        blob_unit = "TB";
    }
    bytes = Number(bytes.toFixed(3)).toString();
    return bytes + " " + blob_unit;
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (elmnt.getElementsByClassName("dragger")) {
      /* if present, the header is where you move the DIV from:*/
    elmnt.getElementsByClassName("dragger")[0].onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

/**
 * Generates a table head
 * @param {DIV} img_div - Target div where image will be loaded
 * @param {INPUT[type=file]} file_input - File input from where image will be
                                          generated
 * @callback ?on_update - Callback called when image is updated with blob data
                          and canvas element as arguments.
 */
function editImage(img_div, file_input, on_update) {
    function h(s) { return document.createElement(s) }

    HTMLElement.prototype.stick = function () {
        for (var i = 0; i < arguments.length; i++) {
            this.appendChild(arguments[i]);
        }
        return this;
    }
    HTMLElement.prototype.setText = function () {
        this.textContent = arguments[0];
        return this;
    }

    var img_id = Math.random().toString(36).split(".").join("");

    var table = h("table")
    , caption = table.createCaption()

    , brightness_input = h("input")
    , td_brightness_value = h("td")

    , contrast_input = h("input")
    , td_contrast_value = h("td")

    , sepia_input = h("input")
    , td_sepia_value = h("td")

    , saturation_input = h("input")
    , td_saturation_value = h("td")

    , width_input = h("input")
    , td_width_value = h("td")

    , height_input = h("input")
    , td_height_value = h("td")

    , td_size_value = h("td")
    , button_update = h("button")
    , a_download = h("a");

    caption.textContent = "Edit Image";
    caption.classList.add("dragger");
    table.classList.add("edit-img");

    button_update.textContent = "Update";

    td_brightness_value.textContent = 0;
    td_contrast_value.textContent = 0;
    td_sepia_value.textContent = 0;
    td_saturation_value.textContent = 0;

    width_input.value = 0;
    width_input.type = "number";
    height_input.value = 0;
    height_input.type = "number";
    (function () {
        for (var i = 0; i < arguments.length; i++) {
            var el = arguments[i];
            el.value = 0;
            el.type = "range";
            el.max = 100;
            el.min = -100;
        }
    })(brightness_input, contrast_input, sepia_input, saturation_input);

    table.style.display = "none";

    table.stick(
        h("tr").stick(h("td").setText("Brightness"),
                      h("td").stick(brightness_input),
                      td_brightness_value),
        h("tr").stick(h("td").setText("Contrast"),
                      h("td").stick(contrast_input),
                      td_contrast_value),
        h("tr").stick(h("td").setText("Sepia"),
                      h("td").stick(sepia_input),
                      td_sepia_value),
        h("tr").stick(h("td").setText("Saturation"),
                      h("td").stick(saturation_input),
                      td_saturation_value),
        h("tr").stick(h("td").setText("Width"),
                      h("td").stick(width_input),
                      td_width_value),
        h("tr").stick(h("td").setText("Height"),
                      h("td").stick(height_input),
                      td_height_value),
        h("tr").stick(h("td").setText("Size"), td_size_value, h("td")),
        h("tr").stick(h("td"), h("td").stick(
            button_update, a_download.stick(h("button").setText("Download"))
        ), h("td"))
    );

    // Define functions
    function imageUpdate() {
        if (file_input.files && file_input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = document.getElementById(img_id);
                if (img) img.remove();
                img = document.createElement("img");
                img.id = img_id;
                img_div.style.display = "none";
                img.src = e.target.result;
                img_div.appendChild(img);
                width_input.value = parseInt(width_input.value) === 0 ?
                    img.width : width_input.value;
                height_input.value = parseInt(height_input.value) === 0 ?
                    img.height : height_input.value;
                Caman(img, imageRender);
                setTimeout(function () {
                    var canvas = document.getElementById(img_id);
                    if (canvas instanceof HTMLImageElement) {
                        return;
                    }
                    var dataURL = canvas.toDataURL("image/jpeg", 1.0); // High quality
                    var blob = dataURItoBlob(dataURL);
                    if (on_update) {
                        on_update(blob, canvas);
                    }
                    td_size_value.textContent = humanReadableSize(blob.size);
                    a_download.href = dataURL;
                    a_download.download = Math.random().toString(36) + ".jpg";
                }, 300);
                img_div.style.display = "revert";
            }
            reader.readAsDataURL(file_input.files[0]);
        }
    }
    function imageRender() {
        this.revert();
        this.resize({
            width: parseInt(width_input.value),
            height: parseInt(height_input.value)
        });
        this.brightness(parseInt(brightness_input.value));
        this.contrast(parseInt(contrast_input.value));
        this.sepia(parseInt(sepia_input.value));
        this.saturation(parseInt(saturation_input.value));
        this.render();
    }

    // Set events
    brightness_input.oninput = function () { td_brightness_value.textContent = this.value }
    contrast_input.oninput = function () { td_contrast_value.textContent = this.value }
    sepia_input.oninput = function () { td_sepia_value.textContent = this.value }
    saturation_input.oninput = function () { td_saturation_value.textContent = this.value }
    button_update.onclick = imageUpdate;
    file_input.onchange = imageUpdate;

    document.getElementsByTagName("body")[0].appendChild(table);

    dragElement(table);

    imageUpdate();

    return table;
}
