(function () {
    // Search functionality begins here
    var parents = document.getElementsByClassName("parent-name")
    var alreadyTicking = false
	function getParent(el) {
		return el.parentElement.parentElement.parentElement
	}
    document.getElementById("search_box").addEventListener("input", function (event) {
        while (alreadyTicking) {console.log(alreadyTicking)}
        alreadyTicking = true
        var value = event.target.value.trim()
        if (value.length === 0) {
            for (var i = 0; i < parents.length; i++) {
                getParent(parents[i]).style.display = "block"
            }
            alreadyTicking = false
            return
        }
        var words = value.toLowerCase().split(/\s+/)
        for (var i = 0; i < parents.length; i++) {
            var text = parents[i].textContent.toLowerCase()
            var parent = getParent(parents[i])
            if (text.includes(value)) {
                parent.style.display = "block"
            } else {
                for (var j = 0; j < words.length; j++) {
                    if (words[j] && !text.includes(words[j])) {
                        parent.style.display = "none"
                        break
                    }
                }
                if (j === words.length) {
                    parent.style.display = "block"
                }
            }
        }
        alreadyTicking = false
    })
})()
