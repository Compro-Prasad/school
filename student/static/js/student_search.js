(function () {
    // Search functionality begins here
    var students = document.getElementsByClassName("student-details")
    var alreadyTicking = false
    document.getElementById("search_box").addEventListener("input", function (event) {
        while (alreadyTicking) {console.log(alreadyTicking)}
        alreadyTicking = true
        var value = event.target.value.trim()
        if (value.length === 0) {
            for (var i = 0; i < students.length; i++) {
                students[i].parentElement.style.display = "block"
            }
            alreadyTicking = false
            return
        }
        var words = value.toLowerCase().split(/\s+/)
        for (var i = 0; i < students.length; i++) {
            var text = students[i].textContent.toLowerCase()
            var parent = students[i].parentElement
            if (text.includes(value)) {
                parent.style.display = "block"
            } else {
                for (var j = 0; j < words.length; j++) {
                    if (words[j] && !text.includes(words[j])) {
                        parent.style.display = "none"
                        break
                    }
                }
                if (j === words.length) {
                    parent.style.display = "block"
                }
            }
        }
        alreadyTicking = false
    })
})()
