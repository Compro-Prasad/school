from django.apps import AppConfig


class StudentProfileConfig(AppConfig):
    name = 'student'
    verbose_name = _('Student')
