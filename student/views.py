import re
from tempfile import TemporaryFile
from typing import Dict, Any

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.db.models import Q
from django.conf import settings
from django.views.decorators.http import require_GET
from django.views.decorators.http import require_POST
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from django.urls import reverse
from urllib.parse import quote_plus, unquote
from django.utils.translation import ugettext as _
from PIL import Image
import numpy as np

from school.decorators import django_login
from .populate import Populate
from .models import (
    Student, Session, ParentProfile, StudentProfile, FeeStructure
)
from .forms import (
    NewStudentTransactionForm, SessionForm, FeeStructureForm,
    NewParentTransactionForm, NewStudentForm, RemarksForm
)


@django_login
def home(request):
    return render(request, "home.html", {"title": _("School")})


@require_http_methods(("GET", "POST"))
@django_login
def promote(request, student_id=None):
    redirect_url = request.GET.get('next') or \
        request.META.get('HTTP_REFERER') or '/'
    redirect_url = redirect(unquote(redirect_url))
    try:
        student = student_id and Student.objects.get(pk=student_id)
    except Student.DoesNotExist:
        messages.error(request, _("Student id %(student_id)s does not exist") % {
            'student_id': student_id
        })
        return redirect(redirect_url)
    if request.method == 'GET':
        form = RemarksForm(student=student)
    elif request.method == 'POST':
        form = RemarksForm(request.POST)
        if form.is_valid():
            student = form.cleaned_data.get("student")
            try:
                remarks = student.promote(remark=form.cleaned_data.get("remarks"))
            except AssertionError:
                messages.error(request, _("Please create a new session first"))
                return redirect("/session/new/")
            if remarks.student_next:
                msg = _("Promoted %(name)s from %(from_std)s to %(to_std)s") % {
                    "name": student.profile.name,
                    "from_std": remarks.student_prev.std,
                    "to_std": remarks.student_next.std
                }
            else:
                msg = _("Promoted %(name)s from %(from_std)s") % {
                    "name": student.profile.name,
                    "from_std": remarks.student_prev.std
                }
            messages.success(request, msg)
            return redirect_url
    return render(request, 'promote.html', {'form': form})


@require_GET
@django_login
def search_page(request):
    title = _("Search Students")
    query = request.GET.get("q")
    who = request.GET.get("who")
    if not (who and query):
        return render(request, "search_student.html", {"title": title})
    query = re.sub(r"\s+", " ", query.strip().lower())
    has_words = re.findall("^[a-z ]+$", query)
    words = query.split()
    if not who:
        error = _("Please select an option")
    elif who not in ("father", "mother", "student"):
        error = _("Invalid value for who: '%(who)s'") % {'who': who}
    elif not query:
        error = _("Please provide a search term")
    elif not has_words:
        error = _("Search term should only have english alphabets")
    else:
        try:
            return eval(f'search_{who}(request, words)')
        except ValueError as err:
            error = str(err)
    return render(request, "search_student.html", {
        "title": title,
        "error": error,
        "value": query
    })


@require_GET
@django_login
def populate_data(_):
    Populate()
    return HttpResponse("Created")


def parent_list(request):
    parents = ParentProfile \
        .objects \
        .order_by('father_name', 'mother_name') \
        .prefetch_related("studentprofile_set__student_set__session")
    return render(request, "parent_list.html", {
        "title": _("Parents"),
        "parents": sorted(parents,
                          key=lambda x: x.studentprofile_set is None)
    })


def search_father(request, words):
    or_query = Q()
    for word in words:
        or_query |= Q(father_name__icontains=word)

    parents = ParentProfile.objects \
                           .filter(or_query, studentprofile__isnull=False) \
                           .distinct() \
                           .prefetch_related("studentprofile_set__student_set__session")

    if not parents:
        raise ValueError(_("No records found"))

    return render(request, "parent_list.html", {
        "title": "Parents",
        "parents": sorted(parents, key=lambda p: p.all_paid()),
        "next": f"/search/?q={request.GET.get('q')}&who=father"
    })


def search_mother(request, words):
    or_query = Q()
    for word in words:
        or_query |= Q(mother_name__icontains=word)

    parents = ParentProfile.objects \
                           .filter(or_query, studentprofile__isnull=False) \
                           .distinct() \
                           .prefetch_related("studentprofile_set__student_set__session")

    if not parents:
        raise ValueError(_("No records found"))

    return render(request, "parent_list.html", {
        "title": "Parents",
        "parents": sorted(parents, key=lambda p: p.all_paid()),
        "next": f"/search/?q={request.GET.get('q')}&who=mother"
    })


def search_student(request, words):
    or_query = Q()
    for word in words:
        or_query |= Q(profile__name__icontains=word)

    students = Student.objects \
                      .filter(or_query) \
                      .order_by("status", "std", "profile__name") \
                      .prefetch_related("profile", "session")

    students = Student.get_all_students_mark_pay(students)

    students = sorted(students, key=lambda s: s.paid)

    if not students:
        raise ValueError(_("No records found"))

    return render(request, "student_list.html", {
        "title": _("Students"),
        "students": students,
        "next": f"/search/?q={request.GET.get('q')}&who=student"
    })


def set_language(request, lang):
    response = redirect(request.META.get('HTTP_REFERER') or '/')
    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang.lower().strip()[:2])
    return response


@require_GET
@django_login
def list_students(request, start, end, std=None):
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(
            request, _("Session %(start)s-%(end)s does not exist") % {
                "start": start,
                "end": end
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')
    if std and std not in Student.CLASSES:
        messages.error(request, _("Standard %(std)s does not exist") % {
            "std": std
        })
        return redirect(request.META.get('HTTP_REFERER') or '/')
    title = ((std and f"{std} ") or "") + _("Students") + f" {start}-{end}"
    students = Student.objects \
                      .filter(session=session) \
                      .order_by("profile__name") \
                      .prefetch_related("profile", "session")
    if std:
        students = students.filter(std=std)
    students = Student.get_all_students_mark_pay(students)
    if std:
        next_url = f"/session/{start}-{end}/{std}/student/list/"
    else:
        next_url = f"/session/{start}-{end}/student/list/"
    return render(request, "student_list.html", {
        "title": title,
        "students": students,
        "next": f"?next={next_url}"
    })


@require_http_methods(("GET", "POST"))
@django_login
def student_transaction_page(request, student_id):
    redirect_url = request.GET.get('next') or "/search/"
    redirect_url = redirect(unquote(redirect_url))
    context: Dict[str, Any] = {"title": _("Student Transaction")}
    student = Student.objects.filter(id=student_id) \
                             .prefetch_related("profile", "session")
    if len(student) == 0:
        messages.error(
            request, _("Student id %(student_id)s does not exist") % {
                "student_id": student_id
            }
        )
        return redirect_url
    student = student[0]
    if student.all_paid():
        messages.info(
            request, _(
                "Student %(name)s with scholar number %(number)s"
                " has paid their fees for session %(session)s"
            ) % {
                "name": student.profile.name,
                "number": student.scholar_number,
                "session": student.session
            }
        )
        return redirect_url
    if request.method == "GET":
        context["form"] = NewStudentTransactionForm(student=student)
        return render(request, "student_transaction.html", context)
    form = context["form"] = NewStudentTransactionForm(request.POST,
                                                       student=student)
    if form.is_valid():
        form.save()
        messages.info(request, _("Please return ₹%(returned)s to %(name)s") % {
            "returned": form.returned,
            "name": student.profile.name
        })
        return redirect_url
    return render(request, "student_transaction.html", context)


@require_GET
def student_new(request):
    form = NewStudentForm()
    return render(request, "student_new.html", {"form": form})


@require_http_methods(("GET", "POST"))
def student_new_in_session_std(request, start, end, std):
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(request, _("Session %(start)s-%(end)s doesn't exist") % {
            "start": start,
            "end": end
        })
        return redirect(request.META.get('HTTP_REFERER') or '/')
    if std not in Student.CLASSES:
        messages.error(request, _("Standard %(std)s does not exist") % {
            "std": std
        })
        return redirect(request.META.get('HTTP_REFERER') or '/')
    if request.method == "GET":
        form = NewStudentForm(session=session, std=std)
    elif request.method == "POST":
        form = NewStudentForm(request.POST)
        if form.is_valid():
            messages.info(request, _("Student %(name)s added in %(std)s") % {
                "name": form.student.profile.name,
                "std": form.student.std
            })
            return redirect({
                _('Submit and Show Profile'): f'/student/{form.student.id}/',
                _('Submit and Add Another'): f'{request.path}',
                _('Submit and Switch'): f'/session/{start}-{end}/',
                None: '/'
            }.get(request.POST.get("submit_value")))
    return render(request, "student_new.html", {"form": form})


@require_http_methods(("GET", "POST"))
def parent_student_new(request, parent_id):
    try:
        parents = ParentProfile.objects.get(id=parent_id)
    except (ParentProfile.DoesNotExist, ValueError):
        messages.error(request, _("Parent id %(parent_id)s does not exist") % {
            "parent_id": parent_id
        })
        return redirect(request.META.get('HTTP_REFERER') or '/')

    if request.method == "GET":
        form = NewStudentForm(parents=parents)
    elif request.method == "POST":
        form = NewStudentForm(request.POST, parents=parents)
        if form.is_valid():
            messages.info(request, _("Student %(name)s added in %(std)s") % {
                "name": form.student.profile.name,
                "std": form.student.std
            })
            return redirect({
                _('Submit and Show Profile'): f'/student/{form.student.id}/',
                _('Submit and Add Another'): f'{request.path}',
                _('Submit and Switch'): f'/parent/list/',
                None: '/'
            }.get(request.POST.get("submit_value")))
    return render(request, "student_new.html", {
        "title": parents,
        "form": form
    })


@require_GET
@django_login
def parent_page(request, profile_id):
    try:
        parent_profile = ParentProfile.objects.get(id=profile_id)
        students = Student.get_all_students_mark_pay(
            Student.objects.filter(profile__parents=parent_profile))
        session_students = {}
        for student in students:
            session = student.session
            if session in session_students:
                session_students[session].append(student)
            else:
                session_students[session] = [student]
        return render(request, "parent_display.html", {
            "title": str(parent_profile),
            "parent": parent_profile,
            "session_students": session_students
        })
    except ParentProfile.DoesNotExist:
        messages.error(request, _("Parent id %(parent_id)s does not exist") % {
            'parent_id': profile_id
        })
        return redirect(request.META.get('HTTP_REFERER') or '/')


@require_GET
@django_login
def student_profile_page(request, profile_id):
    try:
        student_profile = StudentProfile.objects.get(id=profile_id)
        students = Student.get_all_students_mark_pay(student_profile.student_set)
        return render(request, "student_profile_display.html", {
            "title": student_profile.name,
            "student": student_profile,
            "students": students
        })
    except StudentProfile.DoesNotExist:
        messages.error(
            request,
            _("Student with profile id %(profile_id)s does not exist") % {
                'profile_id': profile_id
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')


@require_GET
@django_login
def student_page(request, student_id):
    try:
        student = Student.objects.get(id=student_id)
        return render(request, "student_display.html", {
            "title": student.profile.name,
            "student": student
        })
    except StudentProfile.DoesNotExist:
        messages.error(
            request, _("Student id %(student_id)s does not exist") % {
                "student_id": student_id
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')


@require_GET
def fee_struct(request, start, end):
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(
            request, _("Session %(start)s-%(end)s does not exist") % {
                "start": start,
                "end": end
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')
    fee_structs = FeeStructure.objects.filter(session=session)
    sorted(fee_structs, key=lambda x: Student.CLASS2INT[x.std])
    if len(fee_structs) == 0:
        messages.info(
            request,
            _("No fee structure has been created for %(start)s-%(end)s") % {
                "start": start,
                "end": end
            }
        )
        return render(request, "fee_structure_new.html", {"session": session})
    return render(request, "fee_structure.html", {"fee_structs": fee_structs})


@require_GET
def fee_struct_print(request, start, end):
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(
            request, _("Session %(start)s-%(end)s does not exist") % {
                "start": start,
                "end": end
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')
    fee_structs = FeeStructure.objects.filter(session=session)
    sorted(fee_structs, key=lambda x: Student.CLASS2INT[x.std])
    if len(fee_structs) == 0:
        messages.info(
            request,
            _("No fee structure has been created for %(start)s-%(end)s") % {
                "start": start,
                "end": end
            }
        )
        return render(request, "fee_structure_new.html", {"session": session})
    return render(request, "fee_structure_print.html", {"fee_structs": fee_structs})


@require_http_methods(("GET",))
@django_login
def parent_transaction_select_session(request, parent_id):
    next_ = request.GET.get('next')
    redirect_url = (next_ or request.META.get('HTTP_REFERER') or "/search/")
    redirect_url = redirect(unquote(redirect_url))
    try:
        parent = ParentProfile.objects.get(id=parent_id)
    except (ParentProfile.DoesNotExist, ValueError):
        messages.error(request, _("Parent id %(parent_id)s does not exist") % {
            'parent_id': profile_id
        })
        return redirect_url
    sessions = parent.unpaid_sessions()
    if len(sessions) == 0:
        messages.info(request, _("%(parent)s has paid all fees") % {
            "parent": parent
        })
        return redirect_url
    return render(request, "parent_transaction_select_session.html", {
        "next": next_,
        "parent": parent,
        "sessions": sessions,
        "title": _("Select Session")
    })


@require_http_methods(("GET", "POST"))
@django_login
def parent_transaction_page(request, parent_id, start, end):
    next_ = request.GET.get('next')
    redirect_url = (next_ or request.META.get('HTTP_REFERER') or "/search/")
    redirect_url = redirect(unquote(redirect_url))
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(
            request, _("Session %(start)s-%(end)s does not exist") % {
                "start": start,
                "end": end
            }
        )
        return redirect_url
    try:
        parents = ParentProfile.objects.get(id=parent_id)
    except (ParentProfile.DoesNotExist, ValueError):
        messages.error(request, _("Parent id %(parent_id)s does not exist") % {
            'parent_id': profile_id
        })
        return redirect_url
    students = parents.get_students(session)
    if not students:
        messages.error(
            request, _("No students for %(parents)s in %(session)s") % {
                "parents": parents,
                "session": session
            }
        )
        return redirect_url
    context = {"title": _("Parent Transaction"), "session": session}
    if parents.all_paid():
        messages.info(
            request, _("%(parents)s have paid for all their children") % {
                "parents": parents
            }
        )
        return redirect_url
    if request.method == "GET":
        form = NewParentTransactionForm(parents=parents,
                                        session=session,
                                        students=students)
    else:
        form = NewParentTransactionForm(request.POST,
                                        parents=parents,
                                        session=session,
                                        students=students)
        if form.is_valid():
            form.save()
            messages.info(
                request, _("Please return ₹%(returned)s to %(parents)s") % {
                    "returned": form.returned,
                    "parents": parents
                }
            )
            return redirect_url
    context["form"] = form
    return render(request, "parent_transaction.html", context)


def clean_image(img):
    if img.size > 5242880:
        raise ValueError(_("File size cannot be larger than 5MB"))

    img = Image.open(img)

    if img.height < img.width:
        raise ValueError(_("Landscape image not allowed"))

    if img.height > 3500 or img.width > 3500:
        raise ValueError(_("Image has more than 3500 pixels"))

    if img.height / img.width > 2.5:
        raise ValueError(_("Image slimmer than 2:5 is not allowed"))

    if img.mode in ('RGBA', 'LA'):
        # Remove transparency from image
        background = Image.new(img.mode[:-1], img.size, (0, 100, 100))
        background.paste(img, img.split()[-1])  # omit transparency
        img = background
        print("Removed transparency")

    img = img.convert('RGB')

    # Convert to format that cannot store IPTC/EXIF or comments, i.e. Numpy array
    img = np.array(img)

    # Create new image from the Numpy array
    img = Image.fromarray(img)

    return img


@require_POST
@django_login
def student_image_update(request, student_id):
    is_redirect = not request.GET.get("geturl")
    page = redirect(request.META.get('HTTP_REFERER') or '/')
    img_file = request.FILES.get("image")
    if not img_file:
        error = _("No image was provided")
        messages.error(request, error)
        return page if is_redirect else HttpResponse(f"Error: {error}")
    try:
        img = clean_image(img_file)
    except ValueError as error:
        messages.error(request, error)
        return page if is_redirect else HttpResponse(f"Error: {error}")
    student = StudentProfile.objects.get(pk=student_id)
    with TemporaryFile() as tmp_file:
        img.save(tmp_file, 'jpeg')
        tmp_file.seek(0)
        student.img = img_file
        student.save()
    return page if is_redirect else HttpResponse(student.img.url)


@require_GET
@django_login
def session_display(request, start, end):
    try:
        session = Session.get_session(start, end)
    except (Session.DoesNotExist, ValueError):
        messages.error(
            request, _("Session %(start)s-%(end)s does not exist") % {
                "start": start,
                "end": end
            }
        )
        return redirect(request.META.get('HTTP_REFERER') or '/')
    std_with_students = set(Student.objects
                            .filter(session=session)
                            .values_list('std', flat=True))
    return render(request, "session_display.html", {
        "title": f"Session {session}",
        "session": session,
        "classes": Student.CLASSES,
        "std_with_students": std_with_students
    })


@require_GET
def session_list_page(request):
    return render(request, "session_list.html", {
        "title": "Session",
        "sessions": Session.objects.all(),
    })


@require_http_methods(("GET", "POST"))
@django_login
def create_fee_structure_page(request):
    if request.method == "POST":
        session_form = SessionForm({
            "start": request.POST.get("start"),
            "end": request.POST.get("end")
        })
        fee_forms = [
            FeeStructureForm({
                "std": std,
                "admission_fees": request.POST.get(std + ".adm"),
                "monthly_fees": request.POST.get(std + ".mth"),
                "exam_fees": request.POST.get(std + ".exm")
            })
            for std in Student.CLASSES
        ]
        flag = session_form.is_valid()
        if flag:
            session = session_form.save()
            for form in fee_forms:
                form.data['session'] = session
                flag = flag and form.is_valid()
            if not flag:
                session.delete()
        if flag:  # Flag is modified when we reach here
            for form in fee_forms:
                form.save()
            messages.success(
                request,
                _("Created session %(session)s and"
                  " the respective fee structures") % {
                    "session": session
                }
            )
            return redirect("/session/list/")
    else:
        session_form = SessionForm()
        fee_forms = [
            FeeStructureForm(initial={"std": std})
            for std in Student.CLASSES
        ]
    return render(request, "session_new.html", {
        "title": _("New Session"),
        "session_form": session_form,
        "fee_forms": fee_forms
    })
