from datetime import datetime
import re
import os
from django import forms
from django.db import models
from django.db.models import Sum, Max
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from stdnum.in_ import aadhaar
import stdnum


def validate_aadhaar(value):
    try:
        aadhaar_ = aadhaar.validate(value)
    except stdnum.exceptions.InvalidLength:
        raise ValidationError("Number of digits should be 12.")
    except stdnum.exceptions.InvalidFormat:
        raise ValidationError("Only digits from 0 to 9 are allowed.")
    except stdnum.exceptions.InvalidChecksum:
        raise ValidationError("The Aadhaar number is invalid.")
    except TypeError as exception:
        raise ValidationError(exception.message)
    return aadhaar_


class ParentProfile(models.Model):
    class Meta:
        verbose_name = _('Parent Profile')
        verbose_name_plural = _('Parent Profiles')
    father_name = models.CharField(_("Father's Name"), max_length=512, blank=False)
    father_aadhaar = models.CharField(
        _("Father's Aadhaar Number"),
        blank=True, null=True, max_length=12, validators=[validate_aadhaar]
    )
    mother_name = models.CharField(_("Mother's Name"), max_length=512, blank=False)
    mother_aadhaar = models.CharField(
        _("Mother's Aadhaar Number"),
        blank=True, null=True, max_length=12, validators=[validate_aadhaar]
    )

    @staticmethod
    def get_parent_profile(f_id, m_id, f_n, m_n):
        """Returns parent profile object and whether it needs saving or not"""
        if f_id or m_id:
            if not (f_id and m_id):
                raise ValidationError("Provide aadhaar number for "
                                      "both mother and father")
            parents = ParentProfile.objects.filter(
                father_aadhaar=m_id, mother_aadhaar=f_id)
            if parents:
                raise ValidationError("You have swapped aadhaar numbers")
            parents = ParentProfile.objects.filter(
                father_aadhaar=f_id, mother_aadhaar=m_id)
            if parents:
                return parents[0], False
            return ParentProfile(
                father_name=f_n,
                mother_name=m_n,
                father_aadhaar=f_id,
                mother_aadhaar=m_id
            ), True
        return ParentProfile(father_name=f_n, mother_name=m_n), True

    def __str__(self):
        return "{} and {}".format(self.father_name, self.mother_name)

    def get_children(self, session=None):
        if session:
            return self.studentprofile_set.filter(student__session=session)
        return self.studentprofile_set.all()

    def get_students(self, session):
        return Student.objects.filter(session=session, profile__parents=self)

    def has_child(self, session=None):
        return any(self.get_children(session))

    def unpaid_sessions(self):
        session_ids = {
            s.session.id
            for s in Student.get_all_unpaid(
                    Student.objects.filter(profile__parents=self))
        }
        return Session.objects.filter(id__in=session_ids)

    def all_sessions(self):
        return Session.objects.filter(student__profile__parents=self).distinct()

    def is_current(self):
        return any(self.studentprofile_set.filter(student__status="CURRENT"))

    def all_paid(self, session=None):
        return all(s.all_paid() for s in self.get_children(session))


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        # If the filename already exists, remove it
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class StudentProfile(models.Model):

    class Meta:
        verbose_name = _('Student Profile')
        verbose_name_plural = _('Student Profiles')

    def img_upload(obj, filename):
        return os.path.join(obj.gender, obj.aadhaar[0], str(obj.id)) + ".jpg"

    GENDER_CHOICE = (
        ('Female', _('Female')),
        ('Male', _('Male')),
        ('Other', _('Other'))
    )
    CATEGORY_CHOICE = (
        ('General', _('General')),
        ('Minority', _('Minority')),
        ('OBC', _('OBC')),
        ('PwD', _('PwD')),
        ('SC', _('SC')),
        ('ST', _('ST'))
    )
    aadhaar = models.CharField(
        _("Aadhaar Number"),
        unique=True, max_length=12, validators=[validate_aadhaar]
    )
    name = models.CharField(_("Name"), max_length=512, blank=False)
    img = models.ImageField(_("Image"),
                            blank=True, null=True, upload_to=img_upload,
                            storage=OverwriteStorage())
    parents = models.ForeignKey(ParentProfile, on_delete=models.CASCADE)
    gender = models.CharField(
        _("Gender"),
        choices=GENDER_CHOICE,
        max_length=20,
        blank=False,
        default=GENDER_CHOICE[0][0]
    )
    date_of_birth = models.DateField(_("Birth Date"))
    category = models.CharField(
        _("Category"),
        choices=CATEGORY_CHOICE,
        max_length=64,
        blank=False,
        default=CATEGORY_CHOICE[0][0]
    )
    phone = models.CharField(
        _("Phone Number"),
        max_length=10,
        validators=[RegexValidator(r'^[1-9]\d{9}$')]
    )
    address = models.TextField(_("Address"), blank=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.phone)

    def age(self):
        return datetime.now().year - self.date_of_birth.year

    def her_him(self):
        'her' if self.gender == 'Female' else 'him'

    def get_current(self):
        try:
            return self.student_set.get(status="CURRENT")
        except Student.DoesNotExist:
            pass

    def is_current(self):
        return bool(self.get_current())

    def all_paid(self):
        return all(student.all_paid() for student in self.student_set.all())


class Session(models.Model):
    start = models.DateField(_("Start Date"))
    end = models.DateField(_("End Date"))

    class Meta:
        verbose_name = _('Session')
        verbose_name_plural = _('Sessions')
        unique_together = (('start', 'end'),)
        ordering = ['-start']

    def __str__(self):
        return f'{self.start.strftime("%Y")} - {self.end.strftime("%Y")}'

    def string(self):
        return f'{self.start.strftime("%Y")}-{self.end.strftime("%Y")}'

    @staticmethod
    def get_session(start, end):
        start, end = int(start), int(end)
        return Session.objects.get(start__year=start, end__year=end)

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        start = self.start
        end = self.end
        assert start and end
        errors = {}
        if Session.objects.filter(start__year=start.year).exclude(id=self.id):
            errors["start"] = "Session with the same start year already exists"
        if Session.objects.filter(end__year=end.year).exclude(id=self.id):
            errors["end"] = "Session with the same end year already exists"
        if errors:
            raise ValidationError(errors)
        year_diff = end.year - start.year
        month_diff = end.month - start.month
        if not ((year_diff == 0 and month_diff < 12 and month_diff > 4) or
                (year_diff == 1 and month_diff < 0 and month_diff > -8)):
            raise ValidationError(
                "Number of months should be between 6 and 12(inclusive)"
            )

        # If all months are included then its good thing
        if self.month_count() >= 12:
            return
        # Change is made according to new Transaction architecture
        # TODO: Verify it someway
        invalid_transactions = StudentTransaction \
            .objects.filter(student__session=self) \
                    .values('student') \
                    .annotate(all_months=Sum('months')) \
                    .filter(all_months=12)
        # If we have made transactions for months that are not in current session
        # then raise validation error.
        if invalid_transactions:
            raise ValidationError(
                "Old transaction doesn't lie in the given session range")

    def month_count(self):
        if self.start.month > self.end.month:
            return self.end.month - self.start.month + 13
        else:
            return self.end.month - self.start.month + 1

    def month_series(self):
        tmp = datetime.now().date().replace(day=1)
        if self.start.month <= self.end.month:
            return [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(self.start.month, self.end.month + 1)
            ]
        else:
            return [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(self.start.month, 13)
            ] + [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(1, self.end.month + 1)
            ]

    def all_months(self):
        tmp = datetime.now().date().replace(day=1)
        return [
            tmp.replace(month=month).strftime("%b").upper()
            for month in range(1, 13)
        ]

    def months_not_included(self):
        tmp = datetime.now().date()
        if self.end.month < self.start.month:
            return [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(self.end.month + 1, self.start.month)
            ]
        else:
            return [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(self.end.month + 1, 13)
            ] + [
                tmp.replace(month=month).strftime("%b").upper()
                for month in range(1, self.start.month)
            ]


class NoNextClass(Exception):
    """Exception raised when there is no more next class."""


class InvalidClass(Exception):
    """Exception raise when an invalid class was found."""


class FeeStructure(models.Model):
    FEES = {
        "ADM": "Admission",
        "PRL": "Practical",
        "HLF": "Half Yearly",
        "FIN": "Final",
        "JAN": "January",
        "FEB": "February",
        "MAR": "March",
        "APR": "April",
        "MAY": "May",
        "JUN": "June",
        "JUL": "July",
        "AUG": "August",
        "SEP": "September",
        "OCT": "October",
        "NOV": "November",
        "DEC": "December"
    }
    FEES_SHORT = tuple(FEES.keys())
    FEES_LONG = tuple(FEES.values())
    session = models.ForeignKey(Session, models.CASCADE)
    std = models.CharField(_("Standard"), max_length=5)
    admission_fees = models.PositiveIntegerField(_("Admission Fees"))
    monthly_fees = models.PositiveIntegerField(_("Monthly Fees"))
    exam_fees = models.PositiveIntegerField(_("Exam Fees"))
    practical_fees = models.PositiveIntegerField(
        _("Practical Fees"), null=True, blank=True
    )

    class Meta:
        verbose_name = _('Fee Structure')
        verbose_name_plural = _('Fee Structures')
        unique_together = (("session", "std"),)
        ordering = ['-session__start']

    @staticmethod
    def get_fee_struct(session, std):
        return FeeStructure.objects.get(session=session, std=std)

    def __str__(self):
        return f"{self.session} : {self.std}"

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        errors = {}
        if self.admission_fees and self.admission_fees < 1:
            errors["admission_fees"] = _("Amount should be >= 1")
        if self.monthly_fees and self.monthly_fees < 1:
            errors["monthly_fees"] = _("Amount should be >= 1")
        if self.exam_fees and self.exam_fees < 1:
            errors["exam_fees"] = _("Amount should be >= 1")
        if self.practical_fees:
            if self.practical_fees < 1:
                errors["exam_fees"] = _("Amount should be >= 1")
            if self.std not in ['X', 'XII']:
                errors["practical_fees"] = _(
                    "Practical fees not allowed for %(std)s"
                ) % {'std': self.std}
        if self.std not in Student.CLASSES:
            errors["std"] = _("Invalid standard")
        if errors:
            raise ValidationError(errors)

    def get_fees(self, short):
        if short in FeeStructure.INT2MONTH.values():
            return self.monthly_fees
        if short == "ADM":
            return self.admission_fees
        if short in ["HLF", "FIN"]:
            return self.exam_fees
        if short == "PRL":
            return self.practical_fees

    def month_series(self):
        return self.session.month_series()


class Student(models.Model):
    INT2ROMAN = {
        1: "I",
        2: "II",
        3: "III",
        4: "IV",
        5: "V",
        6: "VI",
        7: "VII",
        8: "VIII",
        9: "IX",
        10: "X",
        11: "XI",
        12: "XII",
    }
    ROMAN2INT = dict((v, k) for k, v in INT2ROMAN.items())
    CLASS2INT = dict(list(ROMAN2INT.items()) + [("LKG", -1), ("UKG", 0)])
    SCHOOL2CLASS = {
        _("Harijan Prathamik Pathsala"): [
            "LKG",
            "UKG",
            INT2ROMAN[1],
            INT2ROMAN[2],
            INT2ROMAN[3],
            INT2ROMAN[4],
            INT2ROMAN[5],
        ],
        _("B. S. D. Barsatiya S. S. P. M."): [
            INT2ROMAN[6],
            INT2ROMAN[7],
            INT2ROMAN[8],
        ],
        _("B. S. D. Barsatiya I. C."): [
            INT2ROMAN[9],
            INT2ROMAN[10],
            INT2ROMAN[11],
            INT2ROMAN[12],
        ],
    }
    CLASS2SCHOOL = {
        clas: school
        for school, classes in SCHOOL2CLASS.items()
        for clas in classes
    }
    SCHOOLS = list(SCHOOL2CLASS.keys())
    CLASSES = [
        clas
        for classes in SCHOOL2CLASS.values()
        for clas in classes
    ]
    STATUS_CHOICES = (
        ("CURRENT", _("CURRENT")),
        ("PASS", _("PASS")),
        ("FAIL", _("FAIL")),
    )
    scholar_number = models.PositiveIntegerField(_("Scholar Number"))
    profile = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    std = models.CharField(_("Standard"), max_length=5)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    status = models.CharField(
        _("Status"),
        choices=STATUS_CHOICES,
        max_length=10,
        default="CURRENT"
    )

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        unique_together = (('profile', 'session'),)
        ordering = ['-session__start', '-scholar_number', 'profile__name']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fee_struct = None
        self._transactions = None

    def __str__(self):
        return "{} - {} - {}".format(
            self.scholar_number, self.profile.name, self.std
        )

    @staticmethod
    def get_all_unpaid(students=None):
        if students is None:
            students = Student.objects
        students = students.annotate(
            admission_pay=Max('studenttransaction__admission_pay'),
            months=Sum('studenttransaction__months'),
            exam1_pay=Max('studenttransaction__exam1_pay'),
            exam2_pay=Max('studenttransaction__exam2_pay'),
            practical_pay=Max('studenttransaction__practical_pay')
        )

        def checker(student):
            return (
                student.months and student.admission_pay and
                student.exam1_pay and student.exam2_pay and
                student.practical_pay
            )
        return [s for s in students
                if not (checker(s) and s.months >= s.month_count())]

    @staticmethod
    def get_all_students_mark_pay(students=None):
        if students is None:
            students = Student.objects
        unpaid_students = Student.get_all_unpaid(students)
        notpaid_students = students.filter(studenttransaction__isnull=True)
        all_students = students.all()
        for student in all_students:
            student.paid = student not in unpaid_students  # Paid all
            student.npay = student in notpaid_students  # Not a single transaction made
        return all_students

    def save(self, *args, **kwargs):
        try:
            if self.status == "CURRENT" and \
               self.profile.student_set.get(status="CURRENT"):
                return
        except Student.DoesNotExist:
            # TODO: Add proper exception handling to let the user know
            pass
        super().save(*args, **kwargs)

    def get_students(self, category='parent_children'):
        if category == 'session':
            return Student.objects.filter(session=self.session)
        if category == 'siblings':
            return Student.objects.filter(
                profile__parents=self.profile.parents,
                session=self.session
            ).exclude(id=self.id)
        if category == 'parent_children':
            return Student.objects.filter(
                profile__parents=self.profile.parents,
                session=self.session
            )
        if category in ['std', 'standard', 'class']:
            return Student.objects.filter(std=self.std)
        if category in ['session_std', 'session_standard', 'session_class']:
            return Student.objects.filter(std=self.std, session=self.session)

    @property
    def fee_struct(self):
        if not self._fee_struct:
            self._fee_struct = FeeStructure.objects.get(std=self.std,
                                                        session=self.session)
        return self._fee_struct

    @property
    def transactions(self):
        if not self._transactions:
            print("Loading transactions into cache")
            self._transactions = self.studenttransaction_set.all()
        return self._transactions

    def month_series(self):
        return self.session.month_series()

    def month_count(self):
        return self.session.month_count()

    def paid_month_count(self):
        return self.studenttransaction_set \
                   .aggregate(Sum('months'))['months__sum'] or 0

    def unpaid_month_count(self):
        return 12 - self.paid_month_count()

    def school(self):
        return Student.CLASS2SCHOOL[self.std]

    def next_class(self):
        if self.std == "XII":
            raise NoNextClass()
        try:
            cur_class_index = Student.CLASSES.index(self.std)
            return Student.CLASSES[cur_class_index + 1]
        except ValueError:
            raise InvalidClass()

    def all_paid(self):
        trns = self.studenttransaction_set.all()
        mth = trns.aggregate(Sum('months'))['months__sum']
        adm = any(trn.admission_pay for trn in trns)
        exm1 = any(trn.exam1_pay for trn in trns)
        exm2 = any(trn.exam2_pay for trn in trns)
        prl = any(trn.practical_pay for trn in trns)
        max_mth = self.month_count()
        return mth == max_mth and exm1 and exm2 and adm and prl

    def promote(self, next_session=None, remark=None):
        # TODO: Create promote page
        if next_session is None:
            next_session = Session.objects.all()[0]
        assert next_session != self.session
        old_school = self.school()
        try:
            new_class = self.next_class()
            new_school = Student.CLASS2SCHOOL[new_class]
            scholar_number = (
                self.scholar_number
                if new_school == old_school
                else
                Student.objects.filter(
                    std__in=Student.SCHOOL2CLASS[new_school]
                ).aggregate(
                    models.Max("scholar_number")
                ).get("scholar_number__max") + 1
            )
            student = Student(
                scholar_number=scholar_number,
                profile=self.profile,
                std=new_class,
                session=next_session,
                status="CURRENT"
            )
        except (NoNextClass, InvalidClass):
            self.status = "PASS"
            self.save()
            remark = Remarks(student_prev=self, note=remark)
        else:
            self.status = "PASS"
            self.save()
            student.save()
            remark = Remarks(
                student_prev=self,
                student_next=student,
                note=remark
            )
        remark.save()
        return remark

    def not_promote(self, next_session):
        return Student(
            scholar_number=self.scholar_number,
            profile=self.profile,
            std=self.std,
            session_id=next_session,
            status="CURRENT"
        )


class Remarks(models.Model):
    student_prev = models.ForeignKey(
        Student, on_delete=models.CASCADE,
        related_name='student_prev'
    )
    student_next = models.ForeignKey(
        Student, on_delete=models.CASCADE,
        related_name='student_next',
        null=True, blank=True
    )
    note = models.TextField(_("Note"), null=True)

    class Meta:
        verbose_name = _('Remarks')
        verbose_name_plural = _('Remarks')

    def summary(self):
        if self.note and len(self.note) > 50:
            return self.note[50] + '...'
        return self.note


def verify_checker(value):
    if ':' in value:
        raise ValidationError(_("Restricted words are not allowed"))
    value = re.sub('[^a-b0-9A-Z_]+', ' ', value).strip().split()
    restricted_words = {'os', 'lambda', 'for', 'if', 'import',
                        'class', 'sys', 'eval'}
    for word in value:
        if word in restricted_words or word.startswith('__'):
            raise ValidationError(
                _("Restricted words are not allowed %(word)s") % {
                    'word': word
                }
            )


class Discount(models.Model):
    short_code = models.CharField(_("Short Code"), max_length=20)
    eligibility_check = models.TextField(validators=[verify_checker])
    auto_apply_check = models.TextField(validators=[verify_checker])
    max_discount_amount = models.PositiveIntegerField(null=True, blank=True)
    min_discount_amount = models.PositiveIntegerField(null=True, blank=True)
    discount = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(100)])
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Discount')
        verbose_name_plural = _('Discounts')
        unique_together = (('short_code', 'session'),)

    def check_eligibility(self, student):
        assert student.session == self.session
        return eval(self.eligibility_check)

    def check_auto_apply(self, student):
        assert student.session == self.session
        return eval(self.auto_apply_check)

    def apply_discount(self, amount):
        value = amount * self.discount / 100.0
        max_amt = self.max_discount_amount
        min_amt = self.min_discount_amount
        if max_amt and value > max_amt:
            value = max_amt
        if min_amt and value < min_amt:
            value = min_amt
        return value

    def __str__(self):
        code = f"{self.short_code} -{self.discount}%"
        if self.max_discount_amount and self.min_discount_amount:
            return f"{code} min ₹{self.min_discount_amount}" \
                f" upto ₹{self.max_discount_amount}"
        if self.min_discount_amount:
            return f"{code} min ₹{self.min_discount_amount}"
        if self.max_discount_amount:
            return f"{code} upto ₹{self.max_discount_amount}"
        return code


class ParentTransaction(models.Model):

    class Meta:
        verbose_name = _('Parent Transaction')
        verbose_name_plural = _('Parent Transactions')

    parents = models.ForeignKey(ParentProfile, on_delete=models.CASCADE)
    discount = models.ForeignKey(Discount, on_delete=models.CASCADE,
                                 null=True, blank=True)
    received = models.PositiveIntegerField(_("Received"))
    returned = models.PositiveIntegerField(_("Returned"))
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    def giver_gave(self):
        # How much the giver actually gave
        return self.received + self.returned

    def paid_for_pretty(self):
        return ", ".join(x.student.profile.name.split()[0]
                         for x in self.studenttransaction_set.all())


class StudentTransaction(models.Model):

    class Meta:
        verbose_name = _('Student Transaction')
        verbose_name_plural = _('Student Transactions')

    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    parent_transaction = models.ForeignKey(
        ParentTransaction, on_delete=models.CASCADE,
        null=True, blank=True
    )
    discount = models.ForeignKey(
        Discount, on_delete=models.CASCADE,
        null=True, blank=True
    )
    received = models.PositiveIntegerField(_("Received"))
    returned = models.PositiveIntegerField(_("Returned"))
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    admission_pay = models.BooleanField(_("Admission Pay"), default=False)
    months = models.PositiveIntegerField(_("Months"), default=0)
    exam1_pay = models.BooleanField(_("Exam 1 Pay"), default=False)
    exam2_pay = models.BooleanField(_("Exam 2 Pay"), default=False)
    practical_pay = models.BooleanField("Practical Pay", default=False)

    def __str__(self):
        return f"{self.student} [{self.paid_for()}]"

    def paid_for_pretty(self):
        values = []
        if self.admission_pay:
            values.append(_("Admission"))
        if self.months == 1:
            values.append(_("%(months)s month") % {'months': self.months})
        elif self.months > 1:
            values.append(_("%(months)s months") % {'months': self.months})
        if self.exam1_pay:
            values.append(_("Exam 1"))
        if self.exam2_pay:
            values.append(_("Exam 2"))
        if self.practical_pay:
            values.append(_("Practical"))
        return ", ".join(str(v) for v in values)

    def paid_for(self):
        values = []
        if self.admission_pay:
            values.append("ADM")
        if self.months:
            values.append(f"{self.months}MTH")
        if self.exam1_pay:
            values.append("EXM1")
        if self.exam2_pay:
            values.append("EXM2")
        if self.practical_pay:
            values.append("PRL")
        return " ".join(values)

    def std(self):
        return self.student.std

    def giver_gave(self):
        # How much the giver actually gave
        return self.received + self.returned

    def adm_paid(self):
        return bool(self.student.studenttransaction_set
                    .objects.filter(admission_pay=True))

    def exm1_paid(self):
        return bool(self.student.studenttransaction_set
                    .objects.filter(exam1_pay=True))

    def exm2_paid(self):
        return bool(self.student.studenttransaction_set
                    .objects.filter(exam2_pay=True))

    def prl_paid(self):
        return bool(self.student.studenttransaction_set
                    .objects.filter(practical_pay=True))

    def mth_paid(self):
        mth = self.student.studenttransaction_set \
                          .aggregate(Sum('months')) \
                          ['months__sum']
        return self.student.month_count() == mth

    def all_paid(self):
        trns = self.student.studenttransaction_set.all()
        mth = trns.aggregate(Sum('months'))['months__sum']
        adm = any(trn.admission_pay for trn in trns)
        exm1 = any(trn.exam1_pay for trn in trns)
        exm2 = any(trn.exam2_pay for trn in trns)
        prl = any(trn.practical_pay for trn in trns)
        max_mth = self.student.session.month_count()
        return mth == max_mth and exm1 and exm2 and adm and prl

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        if self.student.all_paid():
            raise forms.ValidationError(_("All fee is paid"))
