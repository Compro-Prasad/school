import json
from django.contrib import messages


class StudentNotFound(Exception):
    def __init__(self, **kwargs):
        if kwargs:
            values = ", ".join(f"{key}={json.dumps(value)}"
                               for key, value in kwargs.items())
            self.message = f"Student with {values} not found"
        self.message = "Student not found"
        super().__init__(self.message)

    def set_message(self, request):
        messages.error(request, self.message)


class StudentProfileNotFound(Exception):
    def __init__(self, **kwargs):
        if kwargs:
            values = ", ".join(f"{key}={json.dumps(value)}"
                               for key, value in kwargs.items())
            self.message = f"Student profile with {values} not found"
        self.message = "Student profile not found"
        super().__init__(self.message)

    def set_message(self, request):
        messages.error(request, self.message)


class ParentNotFound(Exception):
    def __init__(self, **kwargs):
        if kwargs:
            values = ", ".join(f"{key}={json.dumps(value)}"
                               for key, value in kwargs.items())
            self.message = f"Parent with {values} not found"
        else:
            self.message = "Parent not found"
        super().__init__(self.message)

    def set_message(self, request):
        messages.error(request, self.message)


class StudentAllPaid(Exception):
    def __init__(self, student=None):
        if student:
            self.message = (
                f"Student {student.profile.name} with"
                f" scholar number {student.scholar_number} has paid"
                f" their fees for session {student.session}")
        else:
            self.message = "Student has paid all the fees"
        super().__init__(self.message)

    def set_message(self, request):
        messages.error(request, self.message)


class ParentAllPaid(Exception):
    def __init__(self):
        self.message = "Parent has paid all the fees"
        super().__init__(self.message)

    def set_message(self, request):
        messages.error(request, self.message)
