from datetime import datetime
from django import forms
from django.db.models import Sum
from django.db import IntegrityError, transaction
from django.core.validators import MinValueValidator, RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from .models import ParentProfile, Session, Student, Discount, \
    FeeStructure, StudentProfile, StudentTransaction, \
    ParentTransaction, validate_aadhaar


class RemarksForm(forms.Form):
    remarks = forms.CharField(required=False, widget=forms.Textarea())
    try:
        student = forms.ModelChoiceField(queryset=Student.objects.all())
    except:
        student = forms.ModelChoiceField(queryset=Student.objects.none())

    def __init__(self, *args, **kwargs):
        student = kwargs.pop("student", None)
        if student:
            initial = kwargs.get("initial", {})
            initial["student"] = student
            kwargs["initial"] = initial
        super().__init__(*args, **kwargs)
        if student:
            self.fields['student'].widget = forms.HiddenInput()
            self.fields['student'].initial = student


class StudentForm(forms.ModelForm):
    STD_CHOICES = tuple(
        (
            school,
            tuple((clas, clas) for clas in classes)
        )
        for school, classes in Student.SCHOOL2CLASS.items()
    )
    std = forms.ChoiceField(choices=STD_CHOICES)

    class Meta:
        model = Student
        exclude = ['admission_pay', 'months', 'half_yearly_pay']


def year_choices():
    current_year = datetime.now().year
    first_year = current_year - 25
    return (i for i in range(first_year, current_year + 1))


class NewStudentForm(forms.Form):
    name = forms.CharField(label=_('Name'))
    father_name = forms.CharField(label=_("Father's Name"), max_length=512)
    mother_name = forms.CharField(label=_("Mother's Name"), max_length=512)
    aadhaar = forms.CharField(
        label=_('Aadhaar Number'),
        max_length=12,
        validators=[validate_aadhaar]
    )
    father_aadhaar = forms.CharField(
        label=_("Father's Aadhaar Number"),
        required=False,
        max_length=12,
        validators=[validate_aadhaar]
    )
    mother_aadhaar = forms.CharField(
        label=_("Mother's Aadhaar Number"),
        required=False,
        max_length=12,
        validators=[validate_aadhaar]
    )
    gender = forms.ChoiceField(
        label=_('Gender'),
        choices=StudentProfile.GENDER_CHOICE
    )
    date_of_birth = forms.DateField(
        label=_('Birth Date'),
        widget=forms.NumberInput(attrs={'type': 'date'})
    )
    category = forms.ChoiceField(
        label=_('Category'),
        choices=StudentProfile.CATEGORY_CHOICE
    )
    phone = forms.CharField(
        label=_('Phone Number'),
        max_length=10,
        validators=[RegexValidator(r'^[1-9]\d{9}$')]
    )
    address = forms.CharField(
        label=_('Address'),
        required=False,
        widget=forms.Textarea(attrs={'rows': 5, 'cols': 40})
    )
    scholar_number = forms.IntegerField(
        label=_('Scholar Number'),
        validators=[MinValueValidator(1)]
    )
    std = forms.ChoiceField(
        label=_('Standard'),
        choices=StudentForm.STD_CHOICES
    )
    try:
        session = forms.ModelChoiceField(
            label=_('Session'),
            queryset=Session.objects.all(),
            initial=(Session.objects.all() or (None,))[0]
        )
    except:
        session = forms.ModelChoiceField(
            label=_('Session'),
            queryset=Session.objects.none()
        )
    status = forms.ChoiceField(
        label=_('Status'),
        choices=Student.STATUS_CHOICES,
        initial="CURRENT"
    )

    def __init__(self, *args, **kwargs):
        initial = kwargs.get("initial", {})
        session = kwargs.pop("session", None)
        std = kwargs.pop("std", None)
        self.parents = kwargs.pop("parents", None)
        self.profile = kwargs.pop("profile", None)
        if session:
            initial['session'] = session
        if std:
            initial['std'] = std
        if self.parents:
            parents_values = {
                'father_name': self.parents.father_name,
                'mother_name': self.parents.mother_name,
                'father_aadhaar': self.parents.father_aadhaar,
                'mother_aadhaar': self.parents.mother_aadhaar
            }
            initial.update(parents_values)
        if self.profile:
            profile_values = {
                'name': self.profile.name,
                'aadhaar': self.profile.aadhaar,
                'category': self.profile.category,
                'phone': self.profile.phone,
                'address': self.profile.address,
                'gender': self.profile.gender,
                'date_of_birth': self.profile.date_of_birth
            }
            initial.update(profile_values)
        kwargs['initial'] = initial
        super().__init__(*args, **kwargs)
        if session:
            self.fields['session'].widget = forms.HiddenInput()
        if std:
            self.fields['std'].widget = forms.HiddenInput()
        if self.parents:
            for field in parents_values.keys():
                self.fields[field].widget = forms.HiddenInput()
        if self.profile:
            for field in profile_values.keys():
                self.fields[field].widget = forms.HiddenInput()
        self.student = None

    def clean(self):
        data = super().clean()
        s_no = data.get("scholar_number")
        aadhaar_no = data.get("aadhaar")
        std = data.get("std")
        name = data.get("name")
        if not (s_no and aadhaar_no and std):
            return
        school = Student.CLASS2SCHOOL[std]
        if Student.objects.filter(
                scholar_number=s_no,
                std__in=Student.SCHOOL2CLASS[school]
        ).count() > 0:
            raise ValidationError(f"Student with scholar number {s_no}"
                                  f" already exists in {school}")
        if Student.objects.filter(profile__aadhaar=aadhaar_no).count() > 0:
            raise ValidationError(
                f"Student with aadhaar number {aadhaar_no} already exists")
        if not self.parents:
            self.parents, save_parents = ParentProfile.get_parent_profile(
                f_id=data.get("father_aadhaar"),
                m_id=data.get("mother_aadhaar"),
                f_n=data.get("father_name"),
                m_n=data.get("mother_name"))
            if save_parents:
                self.parents.save()
            else:
                s_profile = StudentProfile \
                    .objects \
                    .filter(name=name, parents=self.parents)
                if s_profile:
                    raise ValidationError(
                        "Student profile already exists. "
                        f"Try promoting {s_profile.her_him()}.")
        if not self.profile:
            self.profile = StudentProfile(
                name=name,
                aadhaar=data.get("aadhaar"),
                parents=self.parents,
                gender=data.get("gender"),
                date_of_birth=data.get("date_of_birth"),
                category=data.get("category"),
                phone=data.get("phone"),
                address=data.get("address")
            )
            try:
                self.profile.save()
            except IntegrityError as error:
                raise ValidationError(f"{error}")
        student = Student(
            profile=self.profile,
            scholar_number=s_no,
            std=std,
            session=data.get("session"),
            status=data.get("status")
        )
        student.save()
        self.student = student


class FeeStructureForm(forms.ModelForm):
    class Meta:
        model = FeeStructure
        exclude = []


class NewParentTransactionForm(forms.ModelForm):
    amount = forms.IntegerField(required=True)

    class Meta:
        model = ParentTransaction
        exclude = ['received', 'returned', 'updated_at', 'created_at']

    def __init__(self, *args, **kwargs):
        parents = kwargs.pop('parents')
        self.session = kwargs.pop('session')
        self.students = kwargs.pop('students', parents.get_children(self.session))
        initial = kwargs.get('initial', {})
        initial['parents'] = parents
        kwargs['initial'] = initial
        super().__init__(*args, **kwargs)
        self.discounts = Discount.objects.none()
        self.fields['parents'].widget = forms.HiddenInput()
        valid_discount_ids = [
            discount.id
            for discount in Discount.objects.filter(session=self.session)
            if all(discount.check_eligibility(s) for s in self.students)
        ]
        if valid_discount_ids:
            self.discounts = Discount.objects.filter(id__in=valid_discount_ids)
            for discount in self.discounts:
                if all(discount.check_auto_apply(s) for s in self.students):
                    self.fields['discount'].initial = discount
                    self.fields['discount'].disabled = True
                    break
        else:
            self.fields['discount'].widget = forms.HiddenInput()
        self.fields['discount'].queryset = self.discounts
        self.student_forms = [
            NewStudentTransactionForm(*args, student=s, prefix=s.id)
            for s in self.students
            if not s.all_paid()
        ]
        for form in self.student_forms:
            form.fail_silently = True
            form.fields['discount'].widget = forms.HiddenInput()
            form.fields['amount'].widget = forms.HiddenInput()
            form.fields['amount'].required = False
        self.student_transactions = []
        self.returned = None
        self.received = None

    def clean_amount(self):
        value = self.cleaned_data.get("amount")
        if value <= 0:
            raise forms.ValidationError("Value should be more than zero")
        return value

    def clean_discount(self):
        value = self.cleaned_data.get('discount')
        if value:
            for student in self.students:
                # Use discount only if all students are eligible
                if not value.check_eligibility(student):
                    raise forms.ValidationError(
                        f'{student} is not eligible for {value}')
        else:
            if not self.discounts:
                self.discounts = Discount.objects.filter(session=self.session)
            for discount in self.discounts:
                flag = True
                for student in self.students:
                    if not (discount.check_eligibility(student) and
                            discount.check_auto_apply(student)):
                        flag = False
                        break
                if flag:  # Use discount only if all students are eligible
                    value = discount
                    break
        return value

    def clean(self, *args, **kwargs):
        data = super().clean(*args, **kwargs)
        total = amt = data.get('amount')
        discount = data.get('discount')
        for form in self.student_forms:
            pk = form.initial['student'].id
            _mutable = form.data._mutable
            form.data._mutable = True
            form.data[f"{pk}-amount"] = str(amt)
            form.data[f"{pk}-discount"] = str(discount.id if discount else "")
            form.data._mutable = _mutable
            if form.is_valid():
                if not form.received:
                    continue
                amt -= form.received
                form.returned = 0
                self.student_transactions.append(form.save(commit=False))
            else:
                self.student_transactions = []
                raise forms.ValidationError(
                    f"{form.cleaned_data['student'].profile.name}"
                    "'s form has error"
                )
        if amt < 0:
            raise forms.ValidationError(
                f"Too little amount. ₹{-amt} more required.")
        if amt == total:
            raise forms.ValidationError("Please select an option")
        self.returned = amt
        self.received = total - amt

    def save(self, commit=True):
        transaction_object = super().save(commit=False)
        transaction_object.returned = self.returned
        transaction_object.received = self.received
        if not commit:
            return transaction_object
        now = datetime.now()
        transaction_object.created_at = now
        transaction_object.updated_at = now
        transaction_object.save()
        for s_transaction in self.student_transactions:
            s_transaction.parent_transaction = transaction_object
            s_transaction.created_at = now
            s_transaction.updated_at = now
            s_transaction.save()
        return transaction_object


class NewStudentTransactionForm(forms.ModelForm):
    amount = forms.IntegerField(required=True)

    class Meta:
        model = StudentTransaction
        exclude = ['received', 'returned', 'updated_at', 'created_at',
                   'parent_transaction']

    def __init__(self, *args, **kwargs):
        student = kwargs.pop('student', None)
        if student:
            initial = kwargs.get('initial', {})
            initial['student'] = student
            kwargs['initial'] = initial
        super().__init__(*args, **kwargs)
        self.discounts = Discount.objects.none()
        if not student:
            return

        if student.std not in ('X', 'XII'):
            self.fields['practical_pay'].widget = forms.HiddenInput()
        self.fields['student'].widget = forms.HiddenInput()
        valid_discount_ids = [
            discount.id
            for discount in Discount.objects.filter(session=student.session)
            if discount.check_eligibility(student)
        ]
        if valid_discount_ids:
            self.discounts = Discount.objects.filter(id__in=valid_discount_ids)
            for discount in self.discounts:
                if discount.check_auto_apply(student):
                    self.fields['discount'].initial = discount
                    self.fields['discount'].disabled = True
                    break
        else:
            self.fields['discount'].widget = forms.HiddenInput()
        self.fields['discount'].queryset = self.discounts
        mth = 0
        for trn in student.studenttransaction_set.all():
            if trn.admission_pay:
                self.fields['admission_pay'].widget = forms.HiddenInput()
            if trn.exam1_pay:
                self.fields['exam1_pay'].widget = forms.HiddenInput()
            if trn.exam2_pay:
                self.fields['exam2_pay'].widget = forms.HiddenInput()
            if trn.practical_pay:
                self.fields['practical_pay'].widget = forms.HiddenInput()
            if trn.months:
                mth += trn.months
        if mth >= student.month_count():
            self.fields['months'].widget = forms.HiddenInput()
        else:
            self.fields['months'].widget.attrs.update({
                "max": student.unpaid_month_count()
            })
        self.returned = None
        self.received = None
        self.fail_silently = False

    def clean_amount(self):
        value = self.cleaned_data.get("amount")
        if not value and self.fail_silently:
            return 0
        if value <= 0:
            raise forms.ValidationError("Value should be more than zero")
        return value

    def clean_admission_pay(self):
        value = self.cleaned_data['admission_pay']
        student = self.cleaned_data['student']
        if value and student.studenttransaction_set.filter(admission_pay=True):
            raise forms.ValidationError('Admission Fees is already paid')
        return value

    def clean_discount(self):
        value = self.cleaned_data.get('discount')
        student = self.cleaned_data['student']
        if value:
            if not value.check_eligibility(student):
                raise forms.ValidationError(
                    f'{student} is not eligible for {value}')
        else:
            if not self.discounts:
                self.discounts = Discount.objects.filter(session=student.session)
            for discount in self.discounts:
                if discount.check_eligibility(student) and \
                   discount.check_auto_apply(student):
                    value = discount
                    break
        return value

    def clean_months(self):
        value = self.cleaned_data['months']
        student = self.cleaned_data['student']
        last_months = student.studenttransaction_set \
                             .aggregate(Sum('months')) \
                             ['months__sum'] or 0
        month_count = student.month_count()
        if value and last_months == month_count:
            raise forms.ValidationError('Monthly Fees is already paid')
        if value + last_months > month_count:
            raise forms.ValidationError('Number of months should not be more than '
                                        f'{month_count-last_months}')
        return value

    def clean_practical_pay(self):
        value = self.cleaned_data['practical_pay']
        student = self.cleaned_data['student']
        if value and student.studenttransaction_set.filter(practical_pay=True):
            raise forms.ValidationError('Practical Fees is already paid')
        return value

    def clean_exam1_pay(self):
        value = self.cleaned_data['exam1_pay']
        student = self.cleaned_data['student']
        if value and student.studenttransaction_set.filter(exam1_pay=True):
            raise forms.ValidationError('Exam1 Fees is already paid')
        return value

    def clean_exam2_pay(self):
        value = self.cleaned_data['exam2_pay']
        student = self.cleaned_data['student']
        if value and student.studenttransaction_set.filter(exam2_pay=True):
            raise forms.ValidationError('Exam2 Fees is already paid')
        return value

    def clean(self, *args, **kwargs):
        data = super().clean(*args, **kwargs)
        total = amt = data.get('amount')
        student = data.get('student')
        discount = data.get('discount')
        mth = data.get('months')
        adm = data.get('admission_pay')
        prl = data.get('practical_pay')
        ex1 = data.get('exam1_pay')
        ex2 = data.get('exam2_pay')
        if student is None or amt is None:
            return
        if not (self.fail_silently or mth or adm or ex1 or ex2 or prl):
            raise forms.ValidationError(
                "Nothing to pay for. Fill in the options.")
        fee = student.fee_struct
        spent = 0
        if mth:
            spent += mth * fee.monthly_fees
        if adm:
            spent += fee.admission_fees
        if prl:
            spent += fee.practical_fees
        if ex1:
            spent += fee.exam_fees
        if ex2:
            spent += fee.exam_fees
        if discount:
            spent -= discount.apply_discount(spent)
        spent = 0 if spent < 0 else spent
        amt -= spent
        if amt < 0:
            raise forms.ValidationError(
                f"Too little amount. ₹{-amt} more required.")
        if not self.fail_silently and not discount and amt == total:
            # If got a discount then amt can go down to 0.
            # But if discount isn't applied but still we didn't get money
            # then there is some bug in the code.
            raise forms.ValidationError(
                "Something bad happened."
                " Set fail_silently on the form object to suppress this.")
        self.returned = amt
        self.received = spent

    def save(self, commit=True):
        transaction_object = super().save(commit=False)
        transaction_object.returned = self.returned
        transaction_object.received = self.received
        if commit:
            transaction_object.save()
        return transaction_object


class DateInput(forms.DateInput):
    input_type = 'date'


class SessionForm(forms.ModelForm):
    class Meta:
        model = Session
        exclude = []
        widgets = {
            'start': DateInput(),
            'end': DateInput()
        }
