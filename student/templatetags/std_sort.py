from django import template
from student.models import Student

register = template.Library()


@register.filter(is_safe=False)
def student_sort_by_std(value):
    return sorted(value, key=lambda x: Student.CLASS2INT[x.std], reverse=True)
