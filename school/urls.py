"""school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from student import views as spviews

urlpatterns = [path(val[0], val[1], name=key) for key, val in {

    'admin': ['admin/', admin.site.urls],

    'fee_table': ['session/<int:start>-<int:end>/fees/', spviews.fee_struct],

    'fee_table_print': ['session/<int:start>-<int:end>/fees/print/',
                        spviews.fee_struct_print],

    'home': ['', spviews.home],

    'parent': ['parent/<int:profile_id>/', spviews.parent_page],

    'parent_list': ['parent/list/', spviews.parent_list],

    'parent_student_new': [
        'parent/<int:parent_id>/student/new/',
        spviews.parent_student_new
    ],

    'parent_trans': [
        'parent/<int:parent_id>/<int:start>-<int:end>/transaction/new/',
        spviews.parent_transaction_page
    ],
    'parent_trans_session': [
        'parent/<int:parent_id>/transaction/selectsession/',
        spviews.parent_transaction_select_session
    ],

    'populate': ['populate/', spviews.populate_data],

    'promote': ['promote/', spviews.promote],
    'promote_student': ['promote/<int:student_id>/', spviews.promote],

    'search': ['search/', spviews.search_page],

    'session': ['session/<int:start>-<int:end>/', spviews.session_display],

    'session_fee_new': ['session/new/', spviews.create_fee_structure_page],

    'session_std_students': [
        'session/<int:start>-<int:end>/<slug:std>/student/list/',
        spviews.list_students],

    'session_students': [
        'session/<int:start>-<int:end>/student/list/',
        spviews.list_students],

    'session_student_new': ['session/<int:start>-<int:end>/<slug:std>/student/new/',
                            spviews.student_new_in_session_std],

    'sessions': ['session/list/', spviews.session_list_page],

    'set_langauage': ['language/<slug:lang>/', spviews.set_language],

    'student': ['student/<int:student_id>/', spviews.student_page],

    'student_new': ['student/new/', spviews.student_new],

    'student_profile': ['student/profile/<int:profile_id>/',
                        spviews.student_profile_page],

    'sprofile_update_img': ['student/profile/<int:student_id>/image/update/',
                            spviews.student_image_update],

    'student_trans': ['student/<int:student_id>/transaction/new/',
                      spviews.student_transaction_page],
}.items()] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
