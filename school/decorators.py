from django.contrib.auth.decorators import login_required


def django_login(view_func):
    return login_required(view_func, login_url="/admin/login/")
