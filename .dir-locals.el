;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((python-mode . ((lsp-pyls-plugins-flake8-max-line-length . 100)))
 (web-mode . ((web-mode-script-padding . 0))))
